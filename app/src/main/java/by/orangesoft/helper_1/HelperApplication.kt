package by.orangesoft.helper_1

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import by.orangesoft.helper_1.constant.ConstantUtil.CHANNEL_1_ID
import by.orangesoft.helper_1.dagger.component.DaggerAppComponent
import dagger.android.DaggerApplication

class HelperApplication : DaggerApplication() {

    @Suppress("MemberVisibilityCanBePrivate")
    val appComponent = DaggerAppComponent.factory().create(this)

    override fun onCreate() {
        super.onCreate()
        createNotificationChannels()
    }

    override fun applicationInjector() = appComponent

    private fun createNotificationChannels() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = getString(R.string.channel_name)
            val descriptionText = "This is Channel 1"
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(CHANNEL_1_ID, name, importance).apply {
                description = descriptionText
            }
            // Register the channel with the system
            val notificationManager: NotificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }
}

