package by.orangesoft.helper_1.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import by.orangesoft.helper_1.R
import by.orangesoft.helper_1.databinding.RecyclerviewTopicItemBinding
import by.orangesoft.helper_1.models.TopicAndLink


class TopicRecyclerViewAdapter internal constructor(val context: Context) :
    RecyclerView.Adapter<TopicRecyclerViewAdapter.TopicVH>() {

    private var topics = emptyList<TopicAndLink>() // Cached copy of topics

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): TopicVH {
        return TopicVH(
            DataBindingUtil.inflate(
                LayoutInflater.from(context),
                R.layout.recyclerview_topic_item,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int = topics.size

    @SuppressLint("DefaultLocale")
    override fun onBindViewHolder(holder: TopicVH, position: Int) {
        holder.recyclerviewTopicItemBinding.something = topics[position]
        holder.recyclerviewTopicItemBinding.recyclerViewCardView.animation =
            AnimationUtils.loadAnimation(context, R.anim.fade_scale_animation)
    }

    inner class TopicVH(val recyclerviewTopicItemBinding: RecyclerviewTopicItemBinding) :
        RecyclerView.ViewHolder(recyclerviewTopicItemBinding.root)

    internal fun setUsers(topics: List<TopicAndLink>) {
        this.topics = topics
        notifyDataSetChanged()
    }
}