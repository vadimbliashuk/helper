package by.orangesoft.helper_1.dagger.modules

import by.orangesoft.helper_1.dagger.annotation.ActivityScope
import by.orangesoft.helper_1.login.activities.LoginActivity
import by.orangesoft.helper_1.ui.MainActivity
import by.orangesoft.helper_1.ui.SplashActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class ActivityModule {

    @ActivityScope
    @ContributesAndroidInjector(modules = [MainActivityFragmentModule::class])
    internal abstract fun contributeMainActivity(): MainActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [LoginActivityFragmentModule::class])
    internal abstract fun contributeLoginActivity(): LoginActivity

    @ActivityScope
    @ContributesAndroidInjector
    internal abstract fun contributeSplashActivity(): SplashActivity

}