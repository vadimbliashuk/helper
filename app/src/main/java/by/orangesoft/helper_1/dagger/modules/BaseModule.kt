package by.orangesoft.helper_1.dagger.modules

import by.orangesoft.helper_1.dagger.base.ViewModelActivity
import by.orangesoft.helper_1.dagger.base.ViewModelFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class BaseModule {

    @ContributesAndroidInjector
    internal abstract fun contributeViewModelActivity(): ViewModelActivity

    @ContributesAndroidInjector
    internal abstract fun contributeViewModelFragment(): ViewModelFragment
}