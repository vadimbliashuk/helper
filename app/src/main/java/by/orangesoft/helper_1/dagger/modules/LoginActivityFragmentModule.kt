package by.orangesoft.helper_1.dagger.modules

import by.orangesoft.helper_1.dagger.annotation.FragmentScope
import by.orangesoft.helper_1.login.fragments.choose.ChooseActionFragment
import by.orangesoft.helper_1.login.fragments.log_in.LogInFragment
import by.orangesoft.helper_1.login.fragments.registration.registration_email.RegistrationFragment
import by.orangesoft.helper_1.login.fragments.registration.registration_user_info.RegistrationUserInfoFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class LoginActivityFragmentModule {

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeChooseActionFragment(): ChooseActionFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeLoginFragment(): LogInFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeRegistrationFragment(): RegistrationFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeRegistrationUserInfoFragment(): RegistrationUserInfoFragment
}
