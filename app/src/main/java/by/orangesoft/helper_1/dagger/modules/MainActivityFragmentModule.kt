package by.orangesoft.helper_1.dagger.modules

import by.orangesoft.helper_1.dagger.annotation.FragmentScope
import by.orangesoft.helper_1.ui.fragments.android.MyAndroidFragment
import by.orangesoft.helper_1.ui.fragments.common.list.ListOfTopicFragment
import by.orangesoft.helper_1.ui.fragments.common.webview.WebViewFragment
import by.orangesoft.helper_1.ui.fragments.home.HomeFragment
import by.orangesoft.helper_1.ui.fragments.kotlin.KotlinFragment
import by.orangesoft.helper_1.ui.fragments.settings.SettingsFragment
import by.orangesoft.helper_1.ui.fragments.sign_out.SignOutFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class MainActivityFragmentModule {

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeMyAndroidFragment(): MyAndroidFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeListOfTopicFragment(): ListOfTopicFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeWebViewFragment(): WebViewFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeHomeFragment(): HomeFragment


    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeKotlinFragment(): KotlinFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeSettingsFragment(): SettingsFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeSignOutFragment(): SignOutFragment
}