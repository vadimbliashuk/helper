package by.orangesoft.helper_1.dagger.modules

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import by.orangesoft.helper_1.dagger.AppViewModelFactory
import by.orangesoft.helper_1.dagger.annotation.ViewModelKey
import by.orangesoft.helper_1.ui.MainActivityViewModel
import by.orangesoft.helper_1.ui.fragments.android.MyAndroidViewModel
import by.orangesoft.helper_1.ui.fragments.common.list.ListOfTopicViewModel
import by.orangesoft.helper_1.ui.fragments.common.webview.WebViewViewModel
import by.orangesoft.helper_1.ui.fragments.home.HomeViewModel
import by.orangesoft.helper_1.ui.fragments.kotlin.KotlinViewModel
import by.orangesoft.helper_1.ui.fragments.settings.SettingsViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import javax.inject.Singleton


@Suppress("unused")
@Module
internal abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(MainActivityViewModel::class)
    internal abstract fun bindMainActivityViewModels(mainActivityViewModel: MainActivityViewModel): ViewModel

    @Singleton
    @Binds
    @IntoMap
    @ViewModelKey(MyAndroidViewModel::class)
    internal abstract fun bindMyAndroidViewModels(myAndroidViewModel: MyAndroidViewModel): ViewModel

    @Singleton
    @Binds
    @IntoMap
    @ViewModelKey(ListOfTopicViewModel::class)
    internal abstract fun bindListOfTopicViewModels(listOfTopicViewModel: ListOfTopicViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(WebViewViewModel::class)
    internal abstract fun bindWebViewViewModels(webViewViewModel: WebViewViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(HomeViewModel::class)
    internal abstract fun bindHomeViewModels(homeViewModel: HomeViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(KotlinViewModel::class)
    internal abstract fun bindKotlinViewModels(kotlinViewModel: KotlinViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SettingsViewModel::class)
    internal abstract fun bindSettingsViewModels(settingsViewModel: SettingsViewModel): ViewModel

    @Binds
    abstract fun bindViewModelFactory(viewModelFactory: AppViewModelFactory?): ViewModelProvider.Factory?
}