package by.orangesoft.helper_1.firestore

import androidx.lifecycle.LiveData
import by.orangesoft.helper_1.models.TopicAndLink
import by.orangesoft.helper_1.models.User

interface FirestoreFetchData {
    fun fetchDataFromFirebaseTopicOfSkills(name: String): LiveData<ArrayList<TopicAndLink>>
    fun fetchDataFromFirebaseSpecialization(name: String): LiveData<ArrayList<TopicAndLink>>
    fun fetchCurrentUserData(uid: String): LiveData<User>
}