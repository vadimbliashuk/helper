package by.orangesoft.helper_1.firestore

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import by.orangesoft.helper_1.models.TopicAndLink
import by.orangesoft.helper_1.models.User
import com.google.firebase.firestore.FirebaseFirestore
import java.util.*
import kotlin.collections.ArrayList

interface FirestoreFetchDataImpl : FirestoreFetchData {

    override fun fetchDataFromFirebaseTopicOfSkills(name: String): LiveData<ArrayList<TopicAndLink>> {
        val listOfJava = MutableLiveData<ArrayList<TopicAndLink>>()
        val db = FirebaseFirestore.getInstance()
        val list = ArrayList<TopicAndLink>()
        val docRef = db.collection(name)
        var treeMap: TreeMap<Int, TopicAndLink>
        docRef.get()
            .addOnSuccessListener { documents ->
                if (documents != null) {
                    Log.d(
                        "FirestoreFetchDataImpl",
                        "DocumentSnapshot data: ${documents.documents.size}"
                    )
                    treeMap = TreeMap()
                    for (documentSnapshot in documents) {
                        val key: String =
                            documentSnapshot.data.keys.toString().replace("[", "").replace("]", "")
                        treeMap[documentSnapshot.id.toInt()] =
                            TopicAndLink(
                                key,
                                documentSnapshot.data.getValue(key) as String,
                                documentSnapshot.id
                            )
                        Log.d("FirestoreFetchDataImpl", key)
                    }
                    for (entry in treeMap) {
                        list.add(entry.value)
                    }
                    listOfJava.value = list
                } else {
                    Log.d("FirestoreFetchDataImpl", "No such document")
                }
            }
            .addOnFailureListener { exception ->
                Log.d("FirestoreFetchDataImpl", "get failed with ", exception)
            }
        return listOfJava
    }

    override fun fetchDataFromFirebaseSpecialization(name: String): LiveData<ArrayList<TopicAndLink>> {
        val listLiveData = MutableLiveData<ArrayList<TopicAndLink>>()
        val db = FirebaseFirestore.getInstance()
        val list = ArrayList<TopicAndLink>()
        val docRef = db.collection(name)
        docRef.get()
            .addOnSuccessListener { document ->
                if (document != null) {
                    for (d in document) {
                        list.add(TopicAndLink(title = d.id))
                    }
                    listLiveData.value = list
                } else {
                    Log.d("FirestoreFetchDataImpl", "No such document")
                }
            }
            .addOnFailureListener { exception ->
                Log.d(
                    "FirestoreFetchDataImpl",
                    "get failed with ",
                    exception
                )
            }
        return listLiveData
    }

    override fun fetchCurrentUserData(uid: String): LiveData<User> {
        val userData = MutableLiveData<User>()
        val db = FirebaseFirestore.getInstance()
        val docRef = db.collection("users").document(uid)

        docRef.get()
            .addOnSuccessListener { document ->
                if (document.exists()) {
                    userData.value = document.toObject(User::class.java)
                } else
                    Log.d("FirestoreFetchDataImpl", "No such document")
            }.addOnFailureListener { exception ->
                Log.d(
                    "FirestoreFetchDataImpl",
                    "get failed with ",
                    exception
                )
            }
        return userData
    }
}