package by.orangesoft.helper_1.firestore

import android.net.Uri
import androidx.lifecycle.LiveData

interface UserInfoRegistration {
    fun userInfoRegistration(username: String?, uri: Uri?): LiveData<Boolean>
}