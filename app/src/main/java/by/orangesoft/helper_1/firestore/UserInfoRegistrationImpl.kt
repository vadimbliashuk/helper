package by.orangesoft.helper_1.firestore

import android.net.Uri
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage

interface UserInfoRegistrationImpl : UserInfoRegistration {
    override fun userInfoRegistration(username: String?, uri: Uri?): LiveData<Boolean> {
        val userLiveData = MutableLiveData<Boolean>()
        val TAG = "UserInfoRegistration"
        val db = FirebaseFirestore.getInstance()
        val filename = FirebaseAuth.getInstance().currentUser!!.uid
        val ref = FirebaseStorage.getInstance().getReference("/images/$filename")
        var urlGlobal = ""
        if (uri != null) {
            ref.putFile(uri)
                .addOnSuccessListener {
                    Log.d("RegisterActivity", "Successfully uploaded image: ${it.metadata?.path}")
                    ref.downloadUrl.addOnSuccessListener { url_ ->
                        urlGlobal = url_.toString()
                        val userInfo = hashMapOf(
                            "username" to username.toString(),
                            "imageLink" to url_.toString()
                        )
                        db.collection("users")
                            .document(FirebaseAuth.getInstance().currentUser!!.uid)
                            .set(userInfo)
                            .addOnSuccessListener {
                                Log.d(
                                    TAG,
                                    "DocumentSnapshot added with ID: ${FirebaseAuth.getInstance().currentUser!!.uid}"
                                )
                                userLiveData.value = true
                            }
                            .addOnFailureListener { e ->
                                Log.w(TAG, "Error adding document", e)
                            }
                    }
                }
        } else {
            val userInfo = hashMapOf(
                "userName" to username,
                "imageLink" to urlGlobal
            )
            db.collection("users")
                .document(FirebaseAuth.getInstance().currentUser!!.uid)
                .set(userInfo)
                .addOnSuccessListener {
                    Log.d(
                        TAG,
                        "DocumentSnapshot added with ID: ${FirebaseAuth.getInstance().currentUser!!.uid}"
                    )
                    userLiveData.value = true
                }
                .addOnFailureListener { e ->
                    Log.w(TAG, "Error adding document", e)
                }
        }
        return userLiveData
    }
}