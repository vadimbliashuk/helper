package by.orangesoft.helper_1.login.activities

import android.graphics.PorterDuff
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import by.orangesoft.helper_1.R

@Suppress("DEPRECATION")
class LoginActivity : AppCompatActivity(R.layout.activity_login) {

    private lateinit var appBarConfig: AppBarConfiguration
    private lateinit var navController: NavController
    private lateinit var toolbar: androidx.appcompat.widget.Toolbar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        toolbar = findViewById(R.id.toolbar_activity_login)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        //supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        toolbar.navigationIcon?.setColorFilter(
            resources.getColor(R.color.colorPrimary),
            PorterDuff.Mode.SRC_ATOP
        )


        navController = Navigation.findNavController(
            this,
            R.id.nav_host_login_fragment
        )

        val topLevelDestinations = setOf(
            R.id.chooseActionFragment

        )
        appBarConfig = AppBarConfiguration(topLevelDestinations)
        navController = findNavController(R.id.nav_host_login_fragment)
        setupActionBarWithNavController(navController, appBarConfig)
    }

    override fun onSupportNavigateUp(): Boolean {
        return findNavController(R.id.nav_host_login_fragment).navigateUp(appBarConfig) || super.onSupportNavigateUp()
    }
}
