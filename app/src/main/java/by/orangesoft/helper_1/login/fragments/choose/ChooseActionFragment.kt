package by.orangesoft.helper_1.login.fragments.choose

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import by.orangesoft.helper_1.R
import kotlinx.android.synthetic.main.choose_action_fragment.*

class ChooseActionFragment : Fragment(R.layout.choose_action_fragment) {

    companion object {
        fun newInstance() = ChooseActionFragment()
    }

    private lateinit var viewModel: ChooseActionViewModel

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(ChooseActionViewModel::class.java)

        btn_sign_in_two_buttons_frag.setOnClickListener {
            findNavController().navigate(R.id.action_chooseActionFragment_to_logInFragment)
        }
        btn_sign_up_two_buttons_frag.setOnClickListener {
            findNavController().navigate(R.id.action_chooseActionFragment_to_registrationFragment)
        }
    }
}
