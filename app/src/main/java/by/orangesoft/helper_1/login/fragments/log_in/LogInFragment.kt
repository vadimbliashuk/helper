package by.orangesoft.helper_1.login.fragments.log_in

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import by.orangesoft.helper_1.R
import by.orangesoft.helper_1.extensions.hideKeyboard
import by.orangesoft.helper_1.ui.MainActivity
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.log_in_fragment.*
import kotlinx.coroutines.runBlocking
import java.util.*
import kotlin.concurrent.schedule

class LogInFragment : Fragment(R.layout.log_in_fragment) {

    companion object {
        fun newInstance() = LogInFragment()
    }

    private lateinit var auth: FirebaseAuth
    private lateinit var viewModel: LogInViewModel

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(LogInViewModel::class.java)
        auth = FirebaseAuth.getInstance()

        btn_login_log_in_fragment.setOnClickListener {
            hideKeyboard()
            group_login_fragment.visibility = View.GONE
            progress_bar_login_fragment.visibility = View.VISIBLE
            Timer().schedule(1000) {
                runBlocking {
                    if (viewModel.signInWithEmailAndPassword(
                            et_email_log_In_fragment,
                            et_password_log_in_fragment
                        )
                    ) {
                        auth.signInWithEmailAndPassword(
                            et_email_log_In_fragment.text.toString(),
                            et_password_log_in_fragment.text.toString()
                        ).addOnSuccessListener {
                            activity!!.finish()
                            startActivity(Intent(context, MainActivity::class.java))
                        }.addOnFailureListener {
                            Toast.makeText(
                                    context,
                                    "User does not exist or Password is incorrect",
                                    Toast.LENGTH_SHORT
                                )
                                .show()
                            et_password_log_in_fragment.text.clear()
                            group_login_fragment.visibility = View.VISIBLE
                            progress_bar_login_fragment.visibility = View.INVISIBLE
                        }
                    }
                }
            }
        }
    }
}
