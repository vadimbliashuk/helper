package by.orangesoft.helper_1.login.fragments.registration.registration_email

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import by.orangesoft.helper_1.R
import by.orangesoft.helper_1.extensions.hideKeyboard
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.registration_fragment.*
import kotlinx.coroutines.runBlocking
import java.util.*
import kotlin.concurrent.schedule

class RegistrationFragment : Fragment(R.layout.registration_fragment) {

    companion object {
        fun newInstance() =
            RegistrationFragment()
    }

    private lateinit var auth: FirebaseAuth
    private lateinit var viewModel: RegistrationViewModel

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(RegistrationViewModel::class.java)
        auth = FirebaseAuth.getInstance()
        btn_next_registration_fragment.setOnClickListener {
            if (viewModel.performRegister(
                    et_email_registration_fragment,
                    et_password_registration_fragment
                )
            ) {
                hideKeyboard()
                group_registration_fragment.visibility = View.GONE
                progress_bar_registration_fragment.visibility = View.VISIBLE
                Timer().schedule(1000) {
                    runBlocking {
                        auth.createUserWithEmailAndPassword(
                            et_email_registration_fragment.text.toString(),
                            et_password_registration_fragment.text.toString()
                        ).addOnCompleteListener { task ->
                            if (task.isSuccessful) {
                                // Sign in success, update UI with the signed-in user's information
                                Log.d(
                                    "_SignUpFragment",
                                    "signInWithEmail:success, User UID ${auth.currentUser!!.uid}"
                                )
                                findNavController().navigate(R.id.action_registrationFragment_to_registrationUserInfoFragment)
                                Toast.makeText(
                                        context,
                                        "User created successfully",
                                        Toast.LENGTH_SHORT
                                    )
                                    .show()
                            } else {
                                // If sign in fails, display a message to the user.
                                Log.w("_SignUpFragment", "signInWithEmail:failure", task.exception)
                                Toast.makeText(
                                    context, "User with this email already created.",
                                    Toast.LENGTH_LONG
                                ).show()
                            }
                        }
                    }
                }
            }
        }
    }
}