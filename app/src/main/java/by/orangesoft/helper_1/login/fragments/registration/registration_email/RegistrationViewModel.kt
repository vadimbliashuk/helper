package by.orangesoft.helper_1.login.fragments.registration.registration_email

import android.util.Patterns
import android.widget.EditText
import androidx.lifecycle.ViewModel

class RegistrationViewModel : ViewModel() {

    fun performRegister(email: EditText, password: EditText): Boolean {
        if (email.text.toString().isEmpty()) {
            email.error = "Please enter Email"
            email.requestFocus()
            return false
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(email.text.toString()).matches()) {
            email.error = "Please enter valid Email"
            email.requestFocus()
            return false
        }
        if (password.text.toString().isEmpty()) {
            password.error = "Please enter password"
            password.requestFocus()
            return false
        }
        if (password.text.toString().length < 6) {
            password.error = "Password must be at least 6 characters"
            password.requestFocus()
            return false
        }
        return true
    }
}
