package by.orangesoft.helper_1.login.fragments.registration.registration_user_info

import android.Manifest
import android.app.Activity.RESULT_OK
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.View
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import by.orangesoft.helper_1.R
import by.orangesoft.helper_1.extensions.hideKeyboard
import by.orangesoft.helper_1.ui.MainActivity
import com.bumptech.glide.Glide
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import kotlinx.android.synthetic.main.registration_user_info_fragment.*

class RegistrationUserInfoFragment : Fragment(R.layout.registration_user_info_fragment) {

    companion object {
        fun newInstance() = RegistrationUserInfoFragment()
        const val GALLERY_REQUEST_CODE = 101
        const val TAG = "PermissionDemo"
        const val READ_STORAGE_REQUEST_CODE = 1000
        const val WRITE_STORAGE_REQUEST_CODE = 1001
    }

    private lateinit var viewModel: RegistrationUserInfoViewModel
    private var globalUri: Uri? = null

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(RegistrationUserInfoViewModel::class.java)

        if (savedInstanceState != null) globalUri?.let { setImage(it) }

        btn_skip.setOnClickListener {
            hideKeyboard()
            group_complete_registration_fragment.visibility = View.GONE
            iv_select_photo_register.visibility = View.INVISIBLE
            progressBar_User_Info_fragment.visibility = View.VISIBLE
            viewModel.commonRegistration(null, null)
                .observe(viewLifecycleOwner, Observer {
                    if (it) {
                        startActivity(Intent(context, MainActivity::class.java))
                        activity!!.finish()
                    }
                })
        }

        btn_select_photo_register.setOnClickListener {
            setupPermissions()
        }

        btn_complete_register.setOnClickListener {
            val username = et_username_register.text.toString()
            hideKeyboard()
            group_complete_registration_fragment.visibility = View.GONE
            iv_select_photo_register.visibility = View.INVISIBLE
            progressBar_User_Info_fragment.visibility = View.VISIBLE

            viewModel.commonRegistration(globalUri, username)
                .observe(viewLifecycleOwner, Observer {
                    if (it) {
                        startActivity(Intent(context, MainActivity::class.java))
                        activity!!.finish()
                    }
                })
        }
    }


    private fun setupPermissions() {
        val permission = ContextCompat.checkSelfPermission(
            activity!!,
            Manifest.permission.READ_EXTERNAL_STORAGE
        )
        val permission_2 = ContextCompat.checkSelfPermission(
            activity!!,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        )

        if (permission != PackageManager.PERMISSION_GRANTED && permission_2 != PackageManager.PERMISSION_GRANTED) {
            Log.i(TAG, "Permission to record denied")
            makeRequest()
        } else {
            pickImageFromGallery()
        }
    }

    private fun makeRequest() {
        requestPermissions(
            arrayOf(
                Manifest.permission.READ_EXTERNAL_STORAGE
            ),
            READ_STORAGE_REQUEST_CODE
        )
        requestPermissions(
            arrayOf(
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ),
            WRITE_STORAGE_REQUEST_CODE
        )

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>, grantResults: IntArray
    ) {
        when (requestCode) {
            READ_STORAGE_REQUEST_CODE -> {
                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Log.i(TAG, "Permission has been denied by user")
                } else {
                    Log.i(TAG, "Permission has been granted by user")
                    pickImageFromGallery()
                }
            }
            WRITE_STORAGE_REQUEST_CODE -> {
                if (grantResults.isEmpty() || grantResults[1] != PackageManager.PERMISSION_GRANTED) {
                    Log.i(TAG, "Permission has been denied by user")
                } else {
                    Log.i(TAG, "Permission has been granted by user")
                    pickImageFromGallery()
                }
            }
        }
    }

    private fun pickImageFromGallery() {
        //Intent to pick image
        val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        intent.type = "image/*"
        val mimeTypes = arrayOf("image/jpeg", "image/png", "image/jpg")
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes)
        intent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
        startActivityForResult(intent, GALLERY_REQUEST_CODE)
    }

    private fun launchImageCrop(uri: Uri) {
        CropImage.activity(uri)
            .setGuidelines(CropImageView.Guidelines.ON)
            .setAspectRatio(500, 500)
            .setCropShape(CropImageView.CropShape.RECTANGLE) // default is rectangle
            .start(context!!, this)
    }

    //handle result of picked image
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            GALLERY_REQUEST_CODE -> {
                if (resultCode == RESULT_OK) {
                    data?.data?.let { uri ->
                        launchImageCrop(uri)
                    }
                } else {
                    Log.e(TAG, "Image selection error: Couldn't select that image from memory.")
                }
            }

            CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE -> {
                val result = CropImage.getActivityResult(data)
                if (resultCode == RESULT_OK) {
                    setImage(result.uri)

                    // iv_select_photo_register.setImageURI(result.uri)
                    globalUri = result.uri
                    btn_select_photo_register.visibility = View.INVISIBLE
                } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                    Log.e(TAG, "Crop error: ${result.error}")
                }
            }
        }
    }

    private fun setImage(uri: Uri) {
        Glide.with(this)
            .load(uri)
            .into(iv_select_photo_register)
    }
}
