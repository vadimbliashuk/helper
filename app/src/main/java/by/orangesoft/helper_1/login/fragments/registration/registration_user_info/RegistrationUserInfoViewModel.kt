package by.orangesoft.helper_1.login.fragments.registration.registration_user_info

import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import by.orangesoft.helper_1.repository.FirebaseRepository
import kotlinx.coroutines.launch

class RegistrationUserInfoViewModel : ViewModel() {
    private val repository: FirebaseRepository = FirebaseRepository()
    lateinit var user_: LiveData<Boolean>

    fun commonRegistration(uri: Uri?, username: String?): LiveData<Boolean> {
        viewModelScope.launch {
            user_ = repository.userInfoRegistration(username, uri)
        }
        return user_
    }
}
