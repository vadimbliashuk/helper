package by.orangesoft.helper_1.models

import android.os.Parcel
import android.os.Parcelable

data class TopicAndLink(val title: String?, var link: String? = "", var idDoc: String? = "") :
    Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(title)
        parcel.writeString(link)
        parcel.writeString(idDoc)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<TopicAndLink> {
        override fun createFromParcel(parcel: Parcel): TopicAndLink {
            return TopicAndLink(parcel)
        }

        override fun newArray(size: Int): Array<TopicAndLink?> {
            return arrayOfNulls(size)
        }
    }
}
