package by.orangesoft.helper_1.models

import javax.inject.Singleton

@Singleton
data class User(var username: String? = null, var imageLink: String? = null) {
}