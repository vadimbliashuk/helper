package by.orangesoft.helper_1.notification

import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import by.orangesoft.helper_1.R
import by.orangesoft.helper_1.constant.ConstantUtil.CHANNEL_1_ID
import by.orangesoft.helper_1.constant.ConstantUtil.REQUEST_CODE_DAILY_NOTIFICATION
import by.orangesoft.helper_1.ui.SplashActivity

class NotificationReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {

        val activityIntent = Intent(context, SplashActivity::class.java)
        val contentIntent = PendingIntent.getActivity(
            context,
            0, activityIntent, 0
        )

        val builder = NotificationCompat.Builder(context!!, CHANNEL_1_ID)
            .setSmallIcon(R.drawable.ic_notifications_active_black_24dp)
            .setContentTitle("Hello!! It's Guide Application!!")
//            .setContentText("If you want to know more about android developing, you should use our Guide App more often")
            .setStyle(
                NotificationCompat.BigTextStyle()
                    .bigText("If you want to know more about android developing, you should use our Guide App more often !!!")
            )
            .setContentIntent(contentIntent)
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            .setAutoCancel(true)
        builder.build()

        with(NotificationManagerCompat.from(context)) {
            // notificationId is a unique int for each notification that you must define
            notify(REQUEST_CODE_DAILY_NOTIFICATION, builder.build())
        }
    }

}
