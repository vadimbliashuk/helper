package by.orangesoft.helper_1.repository

import android.net.Uri
import androidx.lifecycle.LiveData
import by.orangesoft.helper_1.firestore.FirestoreFetchDataImpl
import by.orangesoft.helper_1.firestore.UserInfoRegistrationImpl
import by.orangesoft.helper_1.models.TopicAndLink
import by.orangesoft.helper_1.models.User
import javax.inject.Inject

class FirebaseRepository @Inject constructor() : FirestoreFetchDataImpl, UserInfoRegistrationImpl {

    override fun fetchDataFromFirebaseTopicOfSkills(name: String): LiveData<ArrayList<TopicAndLink>> {
        return super.fetchDataFromFirebaseTopicOfSkills(name)
    }

    override fun fetchDataFromFirebaseSpecialization(name: String): LiveData<ArrayList<TopicAndLink>> {
        return super.fetchDataFromFirebaseSpecialization(name)
    }

    override fun userInfoRegistration(username: String?, uri: Uri?): LiveData<Boolean> {
        return super.userInfoRegistration(username, uri)
    }

    override fun fetchCurrentUserData(uid: String): LiveData<User> {
        return super.fetchCurrentUserData(uid)
    }
}