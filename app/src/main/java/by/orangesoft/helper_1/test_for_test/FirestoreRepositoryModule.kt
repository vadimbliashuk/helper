package by.orangesoft.helper_1.test_for_test

import by.orangesoft.helper_1.repository.FirebaseRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class FirestoreRepositoryModule {

    @Provides
    @Singleton
    fun provideFirestoreRepository(): FirebaseRepository {
        return FirebaseRepository()
    }
}