package by.orangesoft.helper_1.ui

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import androidx.core.view.GravityCompat
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import by.orangesoft.helper_1.R
import by.orangesoft.helper_1.dagger.base.ViewModelActivity
import by.orangesoft.helper_1.notification.NotificationReceiver
import com.bumptech.glide.Glide
import com.google.firebase.auth.FirebaseAuth
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*
import javax.inject.Inject


class MainActivity : ViewModelActivity(), HasAndroidInjector {

    private lateinit var toolbar: androidx.appcompat.widget.Toolbar

    private lateinit var appBarConfig: AppBarConfiguration
    private lateinit var navController: NavController

    @Inject
    lateinit var fragmentInInjector: DispatchingAndroidInjector<Any>
    private val viewModel: MainActivityViewModel by injectViewModels()

    //private lateinit var viewModel: MainActivityViewModel


    private var alarmMgr: AlarmManager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        // MainActivityViewModel is available
        //(applicationContext as HelperApplication).appComponent.inject(this)
        // viewModel =
        //    ViewModelProvider(this).get(MainActivityViewModel(FirebaseRepository())::class.java)

        toolbar = findViewById(R.id.toolbar_activity_main)
        setSupportActionBar(toolbar)

        val calendar: Calendar = Calendar.getInstance().apply {
            timeInMillis = System.currentTimeMillis()
            set(Calendar.HOUR_OF_DAY, 14)
            set(Calendar.MINUTE, 18)
            set(Calendar.SECOND, 1)
        }
        alarmMgr = getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val alarmIntent =
            Intent(applicationContext, NotificationReceiver::class.java).let { intent ->
                PendingIntent.getBroadcast(applicationContext, 0, intent, 0)
            }
        alarmMgr?.setInexactRepeating(
            AlarmManager.RTC_WAKEUP,
            calendar.timeInMillis,
            AlarmManager.INTERVAL_DAY,
            alarmIntent
        )

        alarmMgr?.cancel(alarmIntent)

        navController = Navigation.findNavController(
            this,
            R.id.nav_host_fragment
        )
        navigation_view.setupWithNavController(navController)
        val topLevelDestinations = setOf(
            R.id.nav_home,
            R.id.nav_android,
            R.id.nav_kotlin,
            R.id.nav_sign_out
        )
        appBarConfig = AppBarConfiguration(topLevelDestinations, drawer_layout)
        navController = findNavController(R.id.nav_host_fragment)
        setupActionBarWithNavController(navController, appBarConfig)
        navigation_view.setupWithNavController(navController)

        val headerView = navigation_view.getHeaderView(0)
        val navUsername = headerView.findViewById(R.id.tv_email_nav_header) as TextView
        navUsername.text = FirebaseAuth.getInstance().currentUser?.email

        viewModel.fetchDataAboutUser(FirebaseAuth.getInstance().uid.toString())
            .observe(this, Observer {
                if (it.imageLink != null) {
                    Glide.with(this)
                        .load(it.imageLink)
                        .into(headerView.findViewById(R.id.iv_user_photo) as ImageView)
                }
                if (it.username != null) {
                    headerView.findViewById<TextView>(R.id.tv_username_nav_header).text =
                        it.username
                }
            })

    }

    override fun onSupportNavigateUp(): Boolean {
        return findNavController(R.id.nav_host_fragment).navigateUp(appBarConfig) || super.onSupportNavigateUp()
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun androidInjector(): AndroidInjector<Any> {
        return fragmentInInjector
    }
}