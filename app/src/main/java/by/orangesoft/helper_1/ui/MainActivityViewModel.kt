package by.orangesoft.helper_1.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import by.orangesoft.helper_1.models.User
import by.orangesoft.helper_1.repository.FirebaseRepository
import kotlinx.coroutines.launch
import javax.inject.Inject

class MainActivityViewModel @Inject constructor(private val repository: FirebaseRepository) :
    ViewModel() {

    private lateinit var user: LiveData<User>

    fun fetchDataAboutUser(uid: String): LiveData<User> {
        viewModelScope.launch {
            user = repository.fetchCurrentUserData(uid)
        }
        return user
    }
}