package by.orangesoft.helper_1.ui.fragments.android

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import by.orangesoft.helper_1.R
import by.orangesoft.helper_1.adapter.RecyclerItemClickListener
import by.orangesoft.helper_1.adapter.TopicRecyclerViewAdapter
import by.orangesoft.helper_1.dagger.BaseFragment
import by.orangesoft.helper_1.databinding.FragmentAndroidBinding
import by.orangesoft.helper_1.models.TopicAndLink

class MyAndroidFragment : BaseFragment(),
    RecyclerItemClickListener.OnRecyclerClickListener {

    companion object {
        fun newInstance() =
            MyAndroidFragment()
    }

    private val TAG = "_AndroidFragment"
    private val viewModel: MyAndroidViewModel by injectActivityViewModels()
    private var _binding: FragmentAndroidBinding? = null
    private val binding get() = _binding!!

    private lateinit var topics: ArrayList<TopicAndLink>
    private lateinit var adapter: TopicRecyclerViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState == null) viewModel.fetchData("android")
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentAndroidBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        topics = ArrayList()
        adapter = TopicRecyclerViewAdapter(context!!)
        binding.recyclerViewOfTopic.let { _recyclerView ->
            _recyclerView.layoutManager = LinearLayoutManager(context)
            _recyclerView.adapter = adapter
            _recyclerView.addOnItemTouchListener(
                RecyclerItemClickListener(
                    context!!,
                    _recyclerView,
                    this
                )
            )
        }
        initializeRecyclerView(viewLifecycleOwner)

//        val mGridLayoutManager = GridLayoutManager(context, 3)
//        recyclerViewOfTopic.layoutManager = mGridLayoutManager
    }

    override fun onItemClick(view: View, position: Int) {
        Log.d(TAG, topics[position].title.toString())
        val bundle = bundleOf("topic" to topics[position].title)
        findNavController().navigate(R.id.action_nav_android_to_javaFragment, bundle)
    }

    private fun initializeRecyclerView(owner: LifecycleOwner) {
        viewModel.listOfAndroid.observe(owner, Observer {
            binding.progressBar.visibility = View.GONE
            binding.recyclerViewOfTopic.visibility = View.VISIBLE
            if (topics.isEmpty()) {
                for (i in it) {
                    topics.add(i)
                }
            }
            Log.d(TAG, "List of $topics")
            adapter.setUsers(it)
        })
    }
}