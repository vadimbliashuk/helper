package by.orangesoft.helper_1.ui.fragments.android

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import by.orangesoft.helper_1.models.TopicAndLink
import by.orangesoft.helper_1.repository.FirebaseRepository
import kotlinx.coroutines.launch
import javax.inject.Inject

class MyAndroidViewModel @Inject constructor() :
    ViewModel() {
    private val repository = FirebaseRepository()
    lateinit var listOfAndroid: LiveData<ArrayList<TopicAndLink>>

    fun fetchData(name: String): LiveData<ArrayList<TopicAndLink>> {
        viewModelScope.launch {
            listOfAndroid = repository.fetchDataFromFirebaseSpecialization(name)
        }
        return listOfAndroid
    }
}

