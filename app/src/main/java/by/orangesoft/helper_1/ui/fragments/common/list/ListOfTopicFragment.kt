package by.orangesoft.helper_1.ui.fragments.common.list

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import by.orangesoft.helper_1.R
import by.orangesoft.helper_1.adapter.RecyclerItemClickListener
import by.orangesoft.helper_1.adapter.TopicRecyclerViewAdapter
import by.orangesoft.helper_1.dagger.BaseFragment
import by.orangesoft.helper_1.databinding.FragmentAndroidBinding
import by.orangesoft.helper_1.models.TopicAndLink
import by.orangesoft.helper_1.ui.MainActivity
import kotlinx.android.synthetic.main.fragment_android.*


class ListOfTopicFragment : BaseFragment(),
    RecyclerItemClickListener.OnRecyclerClickListener {

    companion object {
        fun newInstance() =
            ListOfTopicFragment()
    }

    private val TAG = "_ListOfTopicFragment"
    private val viewModel: ListOfTopicViewModel by injectActivityViewModels()
    private var _binding: FragmentAndroidBinding? = null
    private val binding get() = _binding!!


    lateinit var topics: ArrayList<TopicAndLink>
    private lateinit var adapter: TopicRecyclerViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState == null) viewModel.fetchData(
            arguments?.getString("topic").toString()
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        (activity as MainActivity).supportActionBar?.title =
            arguments?.getString("topic").toString()

        _binding = FragmentAndroidBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        topics = ArrayList()
        adapter = TopicRecyclerViewAdapter(context!!)
        binding.recyclerViewOfTopic.let { _recyclerView ->
            _recyclerView.layoutManager = LinearLayoutManager(context)
            _recyclerView.adapter = adapter
            _recyclerView.addOnItemTouchListener(
                RecyclerItemClickListener(
                    context!!,
                    _recyclerView,
                    this
                )
            )
        }
        initializeRecyclerView()
    }

    override fun onItemClick(view: View, position: Int) {
        val bundle = bundleOf(
            "link" to topics[position].link,
            "title" to topics[position].title,
            "id" to topics[position].idDoc,
            "list" to topics
        )
        findNavController().navigate(R.id.action_javaFragment_to_webViewFragment, bundle)
    }

    private fun initializeRecyclerView() {
        viewModel.listOfTopic.observe(viewLifecycleOwner, Observer {
            progressBar.visibility = View.GONE
            recyclerViewOfTopic.visibility = View.VISIBLE
            if (topics.isEmpty()) {
                Log.d(TAG, topics.isEmpty().toString())
                for (i in it) {
                    topics.add(TopicAndLink(i.title, i.link, i.idDoc))
                }
            }
            Log.d("", topics.toString())
            adapter.setUsers(it)
        })
    }
}