package by.orangesoft.helper_1.ui.fragments.common.list

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import by.orangesoft.helper_1.models.TopicAndLink
import by.orangesoft.helper_1.repository.FirebaseRepository
import kotlinx.coroutines.launch
import javax.inject.Inject

const val TAG = "JavaViewModel"

class ListOfTopicViewModel @Inject constructor() : ViewModel() {

    private var repository: FirebaseRepository = FirebaseRepository()
    lateinit var listOfTopic: LiveData<ArrayList<TopicAndLink>>

    fun fetchData(name: String): LiveData<ArrayList<TopicAndLink>>? {
        viewModelScope.launch {
            listOfTopic = repository.fetchDataFromFirebaseTopicOfSkills(name)
            Log.d(TAG, "Test")
        }
        return listOfTopic
    }
}


