package by.orangesoft.helper_1.ui.fragments.common.webview

import android.graphics.Bitmap
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProvider
import by.orangesoft.helper_1.R
import by.orangesoft.helper_1.models.TopicAndLink
import by.orangesoft.helper_1.ui.MainActivity
import kotlinx.android.synthetic.main.web_view_fragment.*
import javax.inject.Inject


class WebViewFragment : Fragment(R.layout.web_view_fragment) {

    private val TAG = "WebViewFragment"

    companion object {
        fun newInstance() =
            WebViewFragment()
    }

    @Inject
    lateinit var factory: ViewModelProvider.Factory
    private val viewModel: WebViewViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setTitleToolbar(arguments?.getString("title").toString())
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        //viewModel = ViewModelProvider(this, factory).get(WebViewViewModel::class.java)
        var urlFromBundle = arguments?.getString("link")
        var globalId = arguments?.getString("id")!!.toInt()
        @Suppress("UNCHECKED_CAST") val list: ArrayList<TopicAndLink> =
            arguments?.get("list") as ArrayList<TopicAndLink>
        if (globalId == list.size) nextViewGone()
        if (globalId == 1) previousViewGone()

        webView.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                view?.loadUrl(url)
                return true
            }

            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                super.onPageStarted(view, url, favicon)
                progressBarForWebView.visibility = View.INVISIBLE
            }
        }
        // webView.settings.javaScriptEnabled = true
        webView.loadUrl(urlFromBundle)
        Log.d(TAG, arguments?.getString("link").toString())


        val listenerNext = View.OnClickListener {
            for (i in list) {
                if (i.idDoc!!.toInt() == globalId + 1) {
                    urlFromBundle = i.link
                    webView.clearHistory()
                    setTitleToolbar(i.title.toString())
                    webView.loadUrl(urlFromBundle)
                }
            }
            globalId += 1
            if (globalId == list.size) nextViewGone()
            if (globalId == 2) previousViewVisible()

            Toast.makeText(this.activity, "Next", Toast.LENGTH_SHORT).show()
        }
        val listenerPrevious = View.OnClickListener {
            for (i in list) {
                if (i.idDoc!!.toInt() == globalId - 1) {
                    urlFromBundle = i.link
                    webView.clearHistory()
                    setTitleToolbar(i.title.toString())
                    webView.loadUrl(urlFromBundle)
                }
            }
            globalId -= 1
            if (globalId == 1) previousViewGone()
            if (globalId == list.size - 1) nextViewVisible()
            Toast.makeText(this.activity, "Previous", Toast.LENGTH_SHORT).show()
        }
        setListener(listenerNext, listenerPrevious)
    }

    private fun setTitleToolbar(title: String) {
        (activity as MainActivity).supportActionBar?.title = title
    }

    private fun nextViewGone() {
        iv_next_web_fragment.visibility = View.GONE
        tv_next_web_fragment.visibility = View.GONE
    }

    private fun previousViewGone() {
        iv_previous_web_fragment.visibility = View.GONE
        tv_previous_web_fragment.visibility = View.GONE
    }

    private fun nextViewVisible() {
        iv_next_web_fragment.visibility = View.VISIBLE
        tv_next_web_fragment.visibility = View.VISIBLE
    }

    private fun previousViewVisible() {
        iv_previous_web_fragment.visibility = View.VISIBLE
        tv_previous_web_fragment.visibility = View.VISIBLE
    }

    private fun setListener(next: View.OnClickListener, previous: View.OnClickListener) {
        // Next
        iv_next_web_fragment.setOnClickListener(next)
        tv_next_web_fragment.setOnClickListener(next)
        // Previous
        iv_previous_web_fragment.setOnClickListener(previous)
        tv_previous_web_fragment.setOnClickListener(previous)
    }
}
