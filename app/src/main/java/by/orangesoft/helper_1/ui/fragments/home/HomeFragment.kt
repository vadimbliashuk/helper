package by.orangesoft.helper_1.ui.fragments.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import by.orangesoft.helper_1.dagger.BaseFragment
import by.orangesoft.helper_1.databinding.HomeFragmentBinding

class HomeFragment : BaseFragment() {

    companion object {
        fun newInstance() = HomeFragment()
    }

    private val viewModel: HomeViewModel by injectActivityViewModels()

    private var _binding: HomeFragmentBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = HomeFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        binding.btnHomeFragmentSetNotification.setOnClickListener {
            sendOnChannel1()
        }
    }

    private fun sendOnChannel1() {
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}
