package by.orangesoft.helper_1.ui.fragments.kotlin

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import by.orangesoft.helper_1.R
import by.orangesoft.helper_1.dagger.BaseFragment
import by.orangesoft.helper_1.databinding.KotlinFragmentBinding


class KotlinFragment : BaseFragment() {

    companion object {
        fun newInstance() = KotlinFragment()
    }

    private val viewModel: KotlinViewModel by injectActivityViewModels()
    private var _binding: KotlinFragmentBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = KotlinFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel._kotlinBasic.observe(viewLifecycleOwner, Observer {
            binding.tvKotlinBasic.text = it;
        })
        viewModel._kotlinAdvanced.observe(viewLifecycleOwner, Observer {
            binding.tvKotlinAdvanced.text = it;
        })
        binding.cardViewKotlinBasic.setOnClickListener {
            findNavController().navigate(R.id.action_nav_kotlin_to_nav_settings)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
