package by.orangesoft.helper_1.ui.fragments.kotlin

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import javax.inject.Inject

class KotlinViewModel @Inject constructor() : ViewModel() {

    val _kotlinBasic = MutableLiveData("Kotlin Basic")
    val _kotlinAdvanced = MutableLiveData("Kotlin \n Advanced")
}
