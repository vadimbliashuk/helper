package by.orangesoft.helper_1.ui.fragments.sign_out

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import by.orangesoft.helper_1.login.activities.LoginActivity
import com.google.firebase.auth.FirebaseAuth

/**
 * A simple [Fragment] subclass.
 */
class SignOutFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        FirebaseAuth.getInstance().signOut()
        startActivity(Intent(context, LoginActivity::class.java))
        activity!!.finish()
    }
}
